package ar.edu.untref.tesis.cadencia.persistencia.daos

import androidx.test.runner.AndroidJUnit4
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MedicionDaoTest : PruebaBasicaDao() {

    private lateinit var dao: MedicionDao

    @Before
    fun setup() {
        dao = bbdd.medicionDao()
    }

    @Test
    fun cuandoSeGuardaUnaUnicaMedicionParaUnaSesionEntoncesLaListaObtenidaTieneUnSoloObjeto() = runBlocking {
        val medicion = Medicion()
        medicion.sesion = 1
        medicion.valor = 150.0
        medicion.tipo = Medicion.Tipo.CADENCIA.name
        medicion.fecha = 1L

        dao.guardar(medicion)
        val guardadas = dao.obtenerMediciones(1)

        Assert.assertEquals("Hay una sola Medición guardada para la Sesión id=1", 1, guardadas.size)
    }

    @Test
    fun cuandoNoExistenMedicionesParaUnaSesionEntoncesLaListaObtenidaEstaVacia() = runBlocking {
        val guardadas = dao.obtenerMediciones(1)
        Assert.assertTrue(guardadas.isEmpty())
    }

    @Test
    fun cuandoSeGuardanDosMedicionesParaUnaSesionEntoncesLaListaTieneDosElementosYEstanEnElOrdenEnElQueSeGuardaron() = runBlocking {
        val medicion1 = Medicion(sesion = 1, valor = 150.0, tipo = Medicion.Tipo.CADENCIA.name, fecha = 1L)
        val medicion2 = Medicion(sesion = 1, valor = 120.0, tipo = Medicion.Tipo.CADENCIA.name, fecha = 2L)

        dao.guardar(medicion1)
        dao.guardar(medicion2)
        val guardadas = dao.obtenerMediciones(1)

        Assert.assertEquals("Hay dos Mediciones guardadas para la Sesión id=1", 2, guardadas.size)
        Assert.assertEquals("La primera Medición tiene valor 150", 150.0, guardadas[0].valor, 0.05)
        Assert.assertEquals("La segunda Medición tiene valor 120", 120.0, guardadas[1].valor, 0.05)
    }

    @Test
    fun cuandoSeGuardanDosMedicionesConElMismoIdEntoncesLaListaObtenidaTieneUnSoloElementoYEsElUltimoEnGuardarse() = runBlocking {
        val medicion1 = Medicion(sesion = 1, valor = 150.0, tipo = Medicion.Tipo.CADENCIA.name, fecha = 1L)
        medicion1.id = 88
        val medicion2 = Medicion(sesion = 1, valor = 120.0, tipo = Medicion.Tipo.CADENCIA.name, fecha = 2L)
        medicion2.id = 88

        dao.guardar(medicion1)
        dao.guardar(medicion2)
        val guardadas = dao.obtenerMediciones(1)

        Assert.assertEquals("Hay una única Medicion guardada para la Sesión id=1", 1, guardadas.size)
        Assert.assertEquals("La segunda Medición tiene valor 120", 120.0, guardadas[0].valor, 0.05)
    }

    @Test
    fun cuandoSeGuardanDosMedicionesDeCadenciaParaUnaSesionEntoncesTiene2MedicionesDeEseTipoY0DeRitmo() = runBlocking {
        val medicion1 = Medicion(sesion = 10, valor = 150.0, tipo = Medicion.Tipo.CADENCIA.name, fecha = 1L)
        medicion1.id = 90
        val medicion2 = Medicion(sesion = 10, valor = 120.0, tipo = Medicion.Tipo.CADENCIA.name, fecha = 2L)
        medicion2.id = 91

        dao.guardar(medicion1)
        dao.guardar(medicion2)
        val cantidadDeMedicionesDeCadencia = dao.cantidadDeMediciones(10, Medicion.Tipo.CADENCIA.name)
        val cantidadDeMedicionesDeRitmo = dao.cantidadDeMediciones(10, Medicion.Tipo.RITMO.name)

        Assert.assertEquals("Hay dos Mediciones de Cadencia", 2, cantidadDeMedicionesDeCadencia)
        Assert.assertEquals("No hay mediciones de Ritmo", 0, cantidadDeMedicionesDeRitmo)
    }
}
