package ar.edu.untref.tesis.cadencia.persistencia.daos

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import ar.edu.untref.tesis.cadencia.persistencia.BaseDeDatos
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
open class PruebaBasicaDao {

    lateinit var bbdd: BaseDeDatos

    @Before
    fun setupBbdd() {
        bbdd = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getInstrumentation().targetContext,
                BaseDeDatos::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @Test
    fun test() {
    }
}
