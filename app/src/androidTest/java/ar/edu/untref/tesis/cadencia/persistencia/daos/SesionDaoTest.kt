package ar.edu.untref.tesis.cadencia.persistencia.daos

import androidx.test.runner.AndroidJUnit4
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SesionDaoTest : PruebaBasicaDao() {

    private lateinit var dao: SesionDao

    @Before
    fun setup() {
        dao = bbdd.sesionDao()
    }

    @Test
    fun cuandoSeGuardaUnaUnicSesionEntoncesLaListaObtenidaTieneUnSoloObjeto() {

        runBlocking {

            val sesion = Sesion()

            dao.guardar(sesion)
            val guardadas = dao.obtenerSesiones()

            Assert.assertEquals("Hay una sola Sesion guardada", 1, guardadas.size)
        }
    }

    @Test
    fun cuandoNoExistenSesionesEntoncesLaListaObtenidaEstaVacia() {

        runBlocking {

            val guardadas = dao.obtenerSesiones()
            Assert.assertTrue(guardadas.isEmpty())
        }
    }

    @Test
    fun cuandoSeGuardanDosSesionesEntoncesLaListaTieneDosElementosYEstanEnElOrdenEnElQueSeGuardaron() {

        runBlocking {

            val sesion1 = Sesion()
            sesion1.id = 1
            val sesion2 = Sesion()
            sesion2.id = 2

            dao.guardar(sesion1)
            dao.guardar(sesion2)
            val guardadas = dao.obtenerSesiones()

            Assert.assertEquals("Hay dos Sesiones guardadas", 2, guardadas.size)
            Assert.assertEquals("La primera Sesion es la id = 1", 1, guardadas[0].id)
            Assert.assertEquals("La segunda Sesion es la id = 2", 2, guardadas[1].id)
        }
    }

    @Test
    fun cuandoSeGuardanDosSesionesConElMismoIdEntoncesLaListaObtenidaTieneUnSoloElemento() {

        runBlocking {

            val sesion1 = Sesion()
            sesion1.id = 12
            val sesion2 = Sesion()
            sesion2.id = 12

            dao.guardar(sesion1)
            dao.guardar(sesion2)
            val guardadas = dao.obtenerSesiones()

            Assert.assertEquals("Hay una única Sesion guardada", 1, guardadas.size)
        }
    }
}
