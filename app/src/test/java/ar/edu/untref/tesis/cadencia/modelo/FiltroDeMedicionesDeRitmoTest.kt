package ar.edu.untref.tesis.cadencia.modelo

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.persistencia.entidades.DatosAdicionales
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class FiltroDeMedicionesDeRitmoTest : PruebaBasica() {

    private lateinit var filtro: FiltroDeMedicionesDeRitmo

    @Before
    fun setup() {
        filtro = FiltroDeMedicionesDeRitmo()
    }

    @Test
    fun `cuando se intenta filtrar una lista vacía entonces se recibe una lista vacía`() {
        val medicionesFiltradas = filtro.filtrar(emptyList())

        Assert.assertTrue("La lista está vacía", medicionesFiltradas.isEmpty())
    }

    @Test
    fun `cuando se intenta filtrar una lista con dos mediciones imprecisas, solo se recibe la primera puesto esa nunca se filtra pero si las siguientes`() {
        val mediciones = mutableListOf(MEDICION_IMPRECISA)
        mediciones.add(MEDICION_IMPRECISA.copy())

        val medicionesFiltradas = filtro.filtrar(mediciones)

        Assert.assertEquals("La lista tiene sólo una Medición", 1, medicionesFiltradas.size)
        Assert.assertEquals("El extra está vacío", "", mediciones.first().extra?.extra)
        Assert.assertEquals("La latitud es 0.0", 0.0, mediciones.first().extra?.latitud)
        Assert.assertEquals("El longitud es 0.0", 0.0, mediciones.first().extra?.longitud)
    }

    @Test
    fun `cuando se intenta filtrar una lista con dos mediciones y una es demasiado lenta, solo se recibe la primera`() {
        val mediciones = listOf(MEDICION_IMPRECISA, MEDICION_DEMASIADO_LENTA)

        val medicionesFiltradas = filtro.filtrar(mediciones)

        Assert.assertEquals("La lista tiene sólo una Medición", 1, medicionesFiltradas.size)
    }

    @Test
    fun `cuando se intenta filtrar una lista con dos mediciones y una es demasiado rapida, solo se recibe la primera`() {
        val mediciones = listOf(MEDICION_IMPRECISA, MEDICION_DEMASIADO_RAPIDA)

        val medicionesFiltradas = filtro.filtrar(mediciones)

        Assert.assertEquals("La lista tiene sólo una Medición", 1, medicionesFiltradas.size)
    }

    @Test
    fun `cuando se intenta filtrar una lista con dos mediciones y una tiene pocos satelites, solo se recibe la primera`() {
        val mediciones = listOf(MEDICION_IMPRECISA, MEDICION_CON_POCOS_SATELITES)

        val medicionesFiltradas = filtro.filtrar(mediciones)

        Assert.assertEquals("La lista tiene sólo una Medición", 1, medicionesFiltradas.size)
    }

    @Test
    fun `cuando se intenta filtrar una lista con dos mediciones correctas, ninguna se filtra, pero el segundo valor no es el mismo porque pasa suavizado por el filtro pasa bajos`() {
        val mediciones = listOf(MEDICION_CORRECTA1, MEDICION_CORRECTA2)

        val medicionesFiltradas = filtro.filtrar(mediciones)

        Assert.assertEquals("La lista tiene ambas Mediciones", 2, medicionesFiltradas.size)
        Assert.assertTrue("No es el valor original", medicionesFiltradas[1].valor == MEDICION_CORRECTA2.valor)
        Assert.assertEquals("El valor filtrado es correcto", 240.0, medicionesFiltradas[1].valor, 0.05)
    }

    companion object {
        val MEDICION_IMPRECISA = Medicion(
                extra = DatosAdicionales(
                        latitud = 0.0,
                        longitud = 0.0,
                        precision = 2.0f,
                        extra = "",
                        satelitesUtilizados = 15
                )
        )

        val MEDICION_DEMASIADO_LENTA = Medicion(
                valor = 500.0,
                extra = DatosAdicionales(
                        latitud = 0.0,
                        longitud = 0.0,
                        precision = 1.0f,
                        extra = "",
                        satelitesUtilizados = 15
                )
        )

        val MEDICION_DEMASIADO_RAPIDA = Medicion(
                valor = 160.0,
                extra = DatosAdicionales(
                        latitud = 0.0,
                        longitud = 0.0,
                        precision = 1.0f,
                        extra = "",
                        satelitesUtilizados = 15
                )
        )

        val MEDICION_CON_POCOS_SATELITES = Medicion(
                valor = 250.0,
                extra = DatosAdicionales(
                        latitud = 0.0,
                        longitud = 0.0,
                        precision = 1.0f,
                        extra = "",
                        satelitesUtilizados = 13
                )
        )

        val MEDICION_CORRECTA1 = Medicion(
                valor = 200.0,
                extra = DatosAdicionales(
                        latitud = 0.0,
                        longitud = 0.0,
                        precision = 1.0f,
                        extra = "",
                        satelitesUtilizados = 15
                )
        )

        val MEDICION_CORRECTA2 = Medicion(
                valor = 300.0,
                extra = DatosAdicionales(
                        latitud = 0.0,
                        longitud = 0.0,
                        precision = 1.0f,
                        extra = "",
                        satelitesUtilizados = 15
                )
        )
    }
}
