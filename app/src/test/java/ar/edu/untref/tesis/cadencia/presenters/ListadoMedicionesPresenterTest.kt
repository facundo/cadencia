package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoListadoMedicionesInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ListadoMedicionesPresenterTest : PruebaBasica() {

    @MockK
    private lateinit var vista: FragmentoListadoMedicionesInterface

    private lateinit var detalle: DetalleSesion
    private lateinit var presenter: ListadoMedicionesPresenter

    @Before
    fun setup() {
        detalle = DetalleSesion(emptyList())
        presenter = ListadoMedicionesPresenter(vista, detalle)
    }

    @Test
    fun `CUANDO se inicia el Presenter ENTONCES se muestran las mediciones`() {
        presenter.iniciar()

        verify(exactly = 1) { vista.mostrarMediciones() }
    }

    @Test
    fun `CUANDO se accede al DetalleSesion que tiene el Presenter SE DEBE obtener el mismo objeto con el que se creo`() {
        val detalleObtenido = presenter.detalleSesion

        Assert.assertEquals(detalle, detalleObtenido)
        Assert.assertTrue(detalleObtenido.mediciones.isEmpty())
    }
}
