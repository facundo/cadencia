package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoResumenInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.modelo.ResumenSesion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.util.FormateadorDeTiempo
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.mockk.verify
import org.junit.Assert
import org.junit.Test

class ResumenPresenterTest : PruebaBasica() {

    @MockK
    private lateinit var vista: FragmentoResumenInterface
    @MockK
    private lateinit var formateador: FormateadorDeTiempo

    private lateinit var detalle: DetalleSesion
    private lateinit var presenter: ResumenPresenter

    @Test
    fun `CUANDO se inicia el Presenter con Mediciones ENTONCES se procesan los datos del ResumenSesion Y se muestran en la vista`() {
        val cienSegundos = 100000000000L
        detalle = DetalleSesion(
                listOf(
                        Medicion(valor = 150.0, sesion = 1, tipo = Medicion.Tipo.CADENCIA.name, fecha = 0L),
                        Medicion(valor = 130.0, sesion = 1, tipo = Medicion.Tipo.CADENCIA.name, fecha = cienSegundos),
                        Medicion(valor = 0.2, sesion = 1, tipo = Medicion.Tipo.RITMO.name, fecha = 0L),
                        Medicion(valor = 0.5, sesion = 1, tipo = Medicion.Tipo.RITMO.name, fecha = cienSegundos)
                )
        )

        presenter = ResumenPresenter(vista, detalle, formateador)
        every { formateador.enMinutos(any()) } returns "01:40"
        val resumenSesionSlot = slot<ResumenSesion>()

        presenter.iniciar()

        verify { vista.mostrarResumen(capture(resumenSesionSlot)) }

        val resumenMostrado = resumenSesionSlot.captured
        Assert.assertEquals("Duración", "01:40", resumenMostrado.duracion)

        Assert.assertEquals("Cadencia maxima", "150 ppm", resumenMostrado.cadenciaMaxima)
        Assert.assertEquals("Cadencia minima", "130 ppm", resumenMostrado.cadenciaMinima)
        Assert.assertEquals("Cadencia promedio", "140.00 ppm", resumenMostrado.cadenciaPromedio)

        // Las velocidades no están calculadas realmente, se ven afectadas por el mock del formateador.
        Assert.assertEquals("Velocidad maxima", "01:40 min/km", resumenMostrado.ritmoMaximo)
        Assert.assertEquals("Velocidad minima", "01:40 min/km", resumenMostrado.ritmoMinimo)
        Assert.assertEquals("Velocidad promedio", "01:40 min/km", resumenMostrado.ritmoPromedio)
    }

    @Test
    fun `CUANDO se inicia el Presenter sin Mediciones ENTONCES se procesan los datos del ResumenSesion con los valores por default Y se muestran en la vista`() {
        detalle = DetalleSesion(emptyList())

        presenter = ResumenPresenter(vista, detalle, formateador)
        every { formateador.enMinutos(any()) } returns "00:00"
        val resumenSesionSlot = slot<ResumenSesion>()

        presenter.iniciar()

        verify { vista.mostrarResumen(capture(resumenSesionSlot)) }

        val resumenMostrado = resumenSesionSlot.captured
        Assert.assertEquals("Duración", "00:00", resumenMostrado.duracion)

        Assert.assertEquals("Cadencia maxima", "0 ppm", resumenMostrado.cadenciaMaxima)
        Assert.assertEquals("Cadencia minima", "0 ppm", resumenMostrado.cadenciaMinima)
        Assert.assertEquals("Cadencia promedio", "0.00 ppm", resumenMostrado.cadenciaPromedio)

        // Las velocidades no están calculadas realmente, se ven afectadas por el mock del formateador.
        Assert.assertEquals("Velocidad maxima", "00:00 min/km", resumenMostrado.ritmoMaximo)
        Assert.assertEquals("Velocidad minima", "00:00 min/km", resumenMostrado.ritmoMinimo)
        Assert.assertEquals("Velocidad promedio", "00:00 min/km", resumenMostrado.ritmoPromedio)
    }
}
