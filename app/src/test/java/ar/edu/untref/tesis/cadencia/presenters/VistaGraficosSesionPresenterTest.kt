package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoVistaGraficosSesionInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.modelo.FiltroDeMedicionesDeRitmo
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class VistaGraficosSesionPresenterTest : PruebaBasica() {

    @MockK(relaxUnitFun = true, relaxed = true)
    private lateinit var vista: FragmentoVistaGraficosSesionInterface
    @MockK(relaxUnitFun = true, relaxed = true)
    private lateinit var filtro: FiltroDeMedicionesDeRitmo
    private lateinit var detalle: DetalleSesion
    private lateinit var presenter: VistaGraficosSesionPresenter

    @Before
    fun setup() {
        detalle = DetalleSesion(mediciones = listOf(Medicion()))
        presenter = VistaGraficosSesionPresenter(vista, detalle, filtro)
    }

    @Test
    fun `CUANDO se prepara el gráfico SE DEBEN dibujar los dos gráficos correspondientes`() {
        presenter.prepararGrafico()

        verify(exactly = 1) { vista.dibujar(R.string.etiqueta_grafico_cadencia, any(), any(), any(), false, any()) }
        verify(exactly = 1) { vista.dibujar(R.string.etiqueta_grafico_ritmo, any(), any(), any(), true, any()) }
    }
}
