package ar.edu.untref.tesis.cadencia

import io.mockk.MockKAnnotations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.setMain
import org.junit.Before

open class PruebaBasica {

    @Before
    fun setupMocks() {
        MockKAnnotations.init(this, relaxed = true, relaxUnitFun = true)
    }

    @Before
    fun iniciarDispatcherDeTest() {
        Dispatchers.setMain(newSingleThreadContext("UI thread"))
    }
}
