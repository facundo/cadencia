package ar.edu.untref.tesis.cadencia.modelo

import android.location.* // ktlint-disable no-wildcard-imports
import ar.edu.untref.tesis.cadencia.PruebaBasica
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ControladorGPSTest : PruebaBasica() {

    @MockK
    private lateinit var locationManager: LocationManager

    @MockK
    private lateinit var listener: ControladorGPS.Listener

    @MockK
    private lateinit var location: Location

    private lateinit var controlador: ControladorGPS

    @Before
    fun setup() {
        controlador = ControladorGPS(locationManager, listener)
    }

    @Test
    fun `cuando se inicia el controlador, el mismo se debe registrar como listener de las ubicaciones con la maxima frecuencia posible y el status del GPS en el locationManager`() {
        controlador.iniciar()

        verify(exactly = 1) { locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, controlador) }
        verify(exactly = 1) { locationManager.addGpsStatusListener(controlador) }
    }

    @Test
    fun `cuando se para el controlador, el mismo se debe desregistrar como listener de ubicaciones y status del gps`() {
        controlador.parar()

        verify(exactly = 1) { locationManager.removeGpsStatusListener(controlador) }
        verify(exactly = 1) { locationManager.removeUpdates(controlador) }
    }

    @Test
    fun `cuando se reciba una ubicacion del GPS nula, no se invoca al listener`() {
        controlador.onLocationChanged(null)

        verify(exactly = 0) { listener.ubicacionRecibida(any()) }
    }

    @Test
    fun `cuando se recibe una ubicacion no nula, se invoca el listener`() {
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.ubicacionRecibida(any()) }
    }

    @Test
    fun `cuando se detecta solo 1 satelite, el listener no recibe la notificacion de que esta listo`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelite1 = mockk<GpsSatellite>()
        every { satelite1.usedInFix() } returns true
        val satelites = listOf(satelite1)
        every { gpsStatus.satellites } returns satelites

        controlador.onGpsStatusChanged(1)

        verify(exactly = 0) { listener.gpsListo() }
    }

    @Test
    fun `cuando se detectan 14 satelites, el listener recibe la notificacion de que esta listo`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }

        controlador.onGpsStatusChanged(1)
        verify(exactly = 1) { listener.gpsListo() }
    }

    @Test
    fun `cuando se detectan 14 satelites y luego el estado de un provider distinto a GPS cambia a OUT_OF_SERVICE, al recibir una nueva ubicacion esta se reporta con 14 satelites`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }
        val slotUbicacion = slot<Ubicacion>()

        controlador.onGpsStatusChanged(1)
        controlador.onStatusChanged(
                provider = "agps",
                status = LocationProvider.OUT_OF_SERVICE,
                extras = null
        )
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.gpsListo() }
        verify { listener.ubicacionRecibida(capture(slotUbicacion)) }
        Assert.assertEquals("Cantidad de satelites", 14, slotUbicacion.captured.satelitesUtilizados)
    }

    @Test
    fun `cuando se detectan 14 satelites y luego el estado del GPS cambia a OUT_OF_SERVICE, al recibir una nueva ubicacion esta se reporta con 0 satelites`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }
        val slotUbicacion = slot<Ubicacion>()

        controlador.onGpsStatusChanged(1)
        controlador.onStatusChanged(
                provider = "gps",
                status = LocationProvider.OUT_OF_SERVICE,
                extras = null
        )
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.gpsListo() }
        verify { listener.ubicacionRecibida(capture(slotUbicacion)) }
        Assert.assertEquals("Cantidad de satelites", 0, slotUbicacion.captured.satelitesUtilizados)
    }

    @Test
    fun `cuando se detectan 14 satelites y luego el estado del GPS cambia a TEMPORARILY_UNAVAILABLE, al recibir una nueva ubicacion esta se reporta con 0 satelites`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }
        val slotUbicacion = slot<Ubicacion>()

        controlador.onGpsStatusChanged(1)
        controlador.onStatusChanged(
                provider = "gps",
                status = LocationProvider.TEMPORARILY_UNAVAILABLE,
                extras = null
        )
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.gpsListo() }
        verify { listener.ubicacionRecibida(capture(slotUbicacion)) }
        Assert.assertEquals("Cantidad de satelites", 0, slotUbicacion.captured.satelitesUtilizados)
    }

    @Test
    fun `cuando se detectan 14 satelites y luego el estado del GPS cambia a AVAILABLE, al recibir una nueva ubicacion esta se reporta con 14 satelites`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }
        val slotUbicacion = slot<Ubicacion>()

        controlador.onGpsStatusChanged(1)
        controlador.onStatusChanged(
                provider = "gps",
                status = LocationProvider.AVAILABLE,
                extras = null
        )
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.gpsListo() }
        verify { listener.ubicacionRecibida(capture(slotUbicacion)) }
        Assert.assertEquals("Cantidad de satelites", 14, slotUbicacion.captured.satelitesUtilizados)
    }

    @Test
    fun `cuando se detectan 14 satelites y luego el provider GPS se desactiva, al recibir una nueva ubicacion esta se reporta con 0 satelites`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }
        val slotUbicacion = slot<Ubicacion>()

        controlador.onGpsStatusChanged(1)
        controlador.onProviderDisabled("gps")
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.gpsListo() }
        verify { listener.ubicacionRecibida(capture(slotUbicacion)) }
        Assert.assertEquals("Cantidad de satelites", 0, slotUbicacion.captured.satelitesUtilizados)
    }

    @Test
    fun `cuando se detectan 14 satelites y luego el provider GPS se reinicia, al recibir una nueva ubicacion esta se reporta con 0 satelites`() {
        val gpsStatus = mockk<GpsStatus>()
        every { locationManager.getGpsStatus(null) } returns gpsStatus
        val satelites = mutableListOf<GpsSatellite>()
        every { gpsStatus.satellites } returns satelites
        for (i in 0 until 14) {
            val satelite = mockk<GpsSatellite>()
            every { satelite.usedInFix() } returns true
            satelites.add(satelite)
        }
        val slotUbicacion = slot<Ubicacion>()

        controlador.onGpsStatusChanged(1)
        controlador.onProviderEnabled("gps")
        controlador.onLocationChanged(location)

        verify(exactly = 1) { listener.gpsListo() }
        verify { listener.ubicacionRecibida(capture(slotUbicacion)) }
        Assert.assertEquals("Cantidad de satelites", 0, slotUbicacion.captured.satelitesUtilizados)
    }
}
