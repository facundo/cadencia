package ar.edu.untref.tesis.cadencia.presenters

import android.location.Location
import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.interfaces.ActividadSesionActivaInterface
import ar.edu.untref.tesis.cadencia.modelo.CalculadoraDeCadencia
import ar.edu.untref.tesis.cadencia.modelo.Ubicacion
import ar.edu.untref.tesis.cadencia.persistencia.Repositorio
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion
import io.mockk.* // ktlint-disable no-wildcard-imports
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SesionActivaPresenterTest : PruebaBasica() {

    private lateinit var presenter: SesionActivaPresenter

    @MockK
    private lateinit var vista: ActividadSesionActivaInterface
    @MockK
    private lateinit var calculadora: CalculadoraDeCadencia
    @MockK
    private lateinit var repositorio: Repositorio

    private val SESION = 1L

    @Before
    fun setup() {
        presenter = SesionActivaPresenter(vista, repositorio, calculadora)
    }

    @Test
    fun `CUANDO se inicia el Presenter sin permisos para acceder a la ubicación SE DEBE pedir permisos para acceder a la ubicacion del usuario`() {
        every { vista.tienePermisosParaAccederAUbicacion() } returns false

        presenter.iniciar()

        verify(exactly = 1) { vista.pedirPermisosParaAccederAUbicacion() }
    }

    @Test
    fun `CUANDO se inicia el Presenter con permisos para acceder a la ubicación SE DEBE empezar a pedir las posiciones al GPS`() {
        every { vista.tienePermisosParaAccederAUbicacion() } returns true

        presenter.iniciar()

        verify(exactly = 1) { vista.empezarAPedirPosicionesAlGPS() }
    }

    @Test
    fun `CUANDO se aceptan los permisos para acceder al GPS SE DEBE empezar a pedir las posiciones al GPS`() {
        presenter.respuestaDelUsuarioAlPedidoDePermisos(true)

        verify(exactly = 1) { vista.empezarAPedirPosicionesAlGPS() }
    }

    @Test
    fun `CUANDO no se aceptan los permisos para acceder al GPS SE DEBE mostrar un mensaje que lo indique`() {
        presenter.respuestaDelUsuarioAlPedidoDePermisos(false)

        verify(exactly = 1) { vista.mostrarMensajeNoHayPermisosParaAccederAUbicacion() }
    }

    @Test
    fun `CUANDO se inicia una sesión SE DEBE mostrar el layout de la sesión activa Y preparar los sensores`() {
        presenter.iniciarSesion()

        verify(exactly = 1) { vista.mostrarLayoutSesionActiva() }
        verify(exactly = 1) { vista.prepararSensores() }
    }

    @Test
    fun `CUANDO se actualiza la cantidad de pasos SE DEBE usar la CalculadoraDeCadencia para obtener el valor instantáneo Y actualizar la vista apropiadamente`() {
        every { calculadora.nuevaMedicionRecibida(any(), any()) } returns 150.0

        presenter.cantidadDePasosActualizada(150, 1000, TestCoroutineDispatcher())

        verify(exactly = 1) { vista.actualizarCadencia("150 ppm") }
    }

    @Test
    fun `CUANDO se actualiza la cantidad de pasos y la cadencia obtenida es distinta a 0 SE DEBE crear una Medicion de Cadencia, guardarla usando el Repositorio y actualizar la vista`() = runBlockingTest {
        every { calculadora.nuevaMedicionRecibida(any(), any()) } returns 150.0
        coEvery { repositorio.nuevaSesion() } returns Sesion(id = SESION)

        presenter.cantidadDePasosActualizada(150, 1000, TestCoroutineDispatcher())

        val medicionEsperada = Medicion()
        medicionEsperada.tipo = Medicion.Tipo.CADENCIA.name
        medicionEsperada.valor = 150.0
        medicionEsperada.fecha = 1000
        medicionEsperada.sesion = SESION

        coVerify(exactly = 1) { repositorio.guardar(medicionEsperada) }
        verify(exactly = 1) { vista.actualizarCadencia("150 ppm") }
    }

    @Test
    fun `CUANDO se actualiza la cantidad de pasos y la cadencia obtenida es 0 NO SE DEBE crear una Medicion de Cadencia ni actualizar la vista`() {
        every { calculadora.nuevaMedicionRecibida(any(), any()) } returns 0.0

        presenter.cantidadDePasosActualizada(150, 1000, TestCoroutineDispatcher())

        val medicionEsperada = Medicion()
        medicionEsperada.tipo = Medicion.Tipo.CADENCIA.name
        medicionEsperada.valor = 0.0
        medicionEsperada.fecha = 1000
        medicionEsperada.sesion = SESION

        coVerify(exactly = 0) { repositorio.guardar(medicionEsperada) }
        verify(exactly = 0) { vista.actualizarCadencia(any()) }
    }

    @Test
    fun `CUANDO se actualizan las ubicaciones SE DEBE ocultar el mensaje que indica que éstas se estaban esperando Y si la sesion está activa SE DEBEN guardar tantas Mediciones como valores de haya`() = runBlockingTest {
        coEvery { repositorio.nuevaSesion() } returns Sesion(id = SESION)
        presenter.iniciarSesion()
        val (cantidad, marcaDeTiempo) = Pair(150, 1L)
        every { calculadora.nuevaMedicionRecibida(cantidad, marcaDeTiempo) } returns 150.0
        presenter.cantidadDePasosActualizada(cantidad, marcaDeTiempo, TestCoroutineDispatcher())

        val location = mockk<Location>(relaxed = true, relaxUnitFun = true)
        every { location.hasSpeed() } returns true
        every { location.speed } returns 0.0f
        every { location.hasAltitude() } returns true
        every { location.altitude } returns 0.0
        every { location.elapsedRealtimeNanos } returns 1
        val ubicacion = Ubicacion(location, 15)

        presenter.ubicacionActualizada(ubicacion, TestCoroutineDispatcher())

        verify(exactly = 1) { vista.ocultarMensajeEsperandoUbicaciones() }

        val slotMedicion = mutableListOf<Medicion>()
        coVerify(exactly = 2) { repositorio.guardar(capture(slotMedicion)) }

        Assert.assertEquals(2, slotMedicion.size)
        Assert.assertEquals(Medicion.Tipo.CADENCIA.name, slotMedicion[0].tipo)
        Assert.assertEquals(Medicion.Tipo.RITMO.name, slotMedicion[1].tipo)
    }
}
