package ar.edu.untref.tesis.cadencia.modelo

import android.location.Location
import ar.edu.untref.tesis.cadencia.PruebaBasica
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert
import org.junit.Test

class ProcesadorDeUbicacionesTest : PruebaBasica() {

    @MockK
    private lateinit var ubicacion: Location
    private val procesador = ProcesadorDeUbicaciones

    @Test
    fun `CUANDO la ubicacion tiene velocidad ENTONCES retorna un objeto Medicion`() {
        every { ubicacion.hasSpeed() } returns true
        val velocidadEnMetrosPorSegundo = 0.5f
        every { ubicacion.speed } returns velocidadEnMetrosPorSegundo
        every { ubicacion.elapsedRealtimeNanos } returns 1
        val ubicacionSensada = Ubicacion(ubicacion, 10)

        val medicion = procesador.crearMedicionDeRitmo(ubicacionSensada, SESION_ID)

        Assert.assertNotNull(medicion)
    }

    @Test
    fun `CUANDO la ubicacion NO tiene velocidad ENTONCES retorna un objeto nulo`() {
        every { ubicacion.hasSpeed() } returns false
        val ubicacionSensada = Ubicacion(ubicacion, 10)

        val medicion = procesador.crearMedicionDeRitmo(ubicacionSensada, SESION_ID)

        Assert.assertNull(medicion)
    }

    companion object {
        private const val SESION_ID = 1L
    }
}
