package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.interfaces.ActividadDetalleSesionInterface
import ar.edu.untref.tesis.cadencia.persistencia.Repositorio
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import java.util.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class DetalleSesionPresenterTest : PruebaBasica() {

    @MockK
    private lateinit var vista: ActividadDetalleSesionInterface

    @MockK
    private lateinit var repositorio: Repositorio

    private lateinit var presenter: DetalleSesionPresenter

    @Before
    fun setup() {
        presenter = DetalleSesionPresenter(vista, SESION_ID, repositorio)
    }

    @Test
    fun `CUANDO se inicia el presenter ENTONCES se piden las mediciones al Repositorio Y se le pasa un DetalleSesion a la vista`() = runBlockingTest {
        val medicionesExistentes = ArrayList<Medicion>()
        medicionesExistentes.add(Medicion())
        coEvery { repositorio.obtenerMediciones(SESION_ID) } returns medicionesExistentes

        presenter.iniciar(TestCoroutineDispatcher())

        verify(exactly = 1) { vista.prepararSecciones(any()) }
    }

    companion object {
        private const val SESION_ID: Long = 1
    }
}
