package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.interfaces.ActividadSesionesInterface
import ar.edu.untref.tesis.cadencia.persistencia.Repositorio
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SesionesPresenterTest : PruebaBasica() {

    private lateinit var presenter: SesionesPresenter
    @MockK
    private lateinit var vista: ActividadSesionesInterface
    @MockK
    private lateinit var repositorio: Repositorio

    @Before
    fun setup() {
        presenter = SesionesPresenter(vista, repositorio)
    }

    @Test
    fun `CUANDO se inicia el Presenter pero no hay sesiones se muestra el mensaje de que no hay sesiones`() = runBlockingTest {
        coEvery { repositorio.obtenerSesiones() } returns emptyList()

        presenter.iniciar(TestCoroutineDispatcher())

        verify(exactly = 1) { vista.cargando(true) }
        verify(exactly = 1) { vista.cargando(false) }
        verify(exactly = 1) { vista.mostrarMensajeNoHaySesiones() }
    }

    @Test
    fun `CUANDO se inicia el Presenter y hay sesiones se muestra el listado`() = runBlockingTest {
        coEvery { repositorio.obtenerSesiones() } returns listOf(Sesion())

        presenter.iniciar(TestCoroutineDispatcher())

        verify(exactly = 1) { vista.cargando(true) }
        verify(exactly = 1) { vista.cargando(false) }
        verify(exactly = 1) { vista.mostrarSesiones() }
    }

    @Test
    fun `CUANDO se piden las Sesiones antes de iniciar el Presenter ENTONCES no hay Sesiones`() {
        Assert.assertTrue(presenter.sesiones.isEmpty())
    }

    @Test
    fun `CUANDO se piden las Sesiones luego de que el Repositorio retorne las Sesiones ENTONCES retorna exactamente esa lista Y se deja de mostrar la vista de carga`() {
        val sesiones = ArrayList<Sesion>()
        sesiones.add(Sesion())
        sesiones.add(Sesion())
        sesiones.add(Sesion())

        presenter.sesionesObtenidas(sesiones)

        Assert.assertEquals(3, presenter.sesiones.size)
        verify(exactly = 1) { vista.cargando(false) }
        verify(exactly = 1) { vista.mostrarSesiones() }
    }

    @Test
    fun `CUANDO se clickea sobre una Sesion del listado ENTONCES se Va al Detalle de dicha sesion`() {
        val creada = Sesion()
        creada.id = 20
        presenter.sesionesObtenidas(listOf(creada))

        presenter.sesionClickeada(0)

        verify(exactly = 1) { vista.irADetalleSesion(creada.id) }
    }
}
