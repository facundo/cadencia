package ar.edu.untref.tesis.cadencia.modelo

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CalculadoraDeCadenciaTest {

    private lateinit var calculadora: CalculadoraDeCadencia

    @Before
    fun setup() {
        calculadora = CalculadoraDeCadencia()
    }

    @Test
    fun `CUANDO se recibe la primera medicion ENTONCES la cadencia calculada es 0`() {

        val resultado = calculadora.nuevaMedicionRecibida(10, 10)

        Assert.assertEquals("La cadencia calculada debe ser cero", 0.0, resultado, 0.05)
    }

    @Test
    fun `CUANDO se reciben dos mediciones en más de 10 segundos ENTONCES la cadencia instantanea calculada ya no es 0`() {

        calculadora.nuevaMedicionRecibida(1, 1)

        val unMinutoEnNanosegundos = 60000000000L
        val resultado = calculadora.nuevaMedicionRecibida(151, unMinutoEnNanosegundos + 1)

        Assert.assertEquals("La cadencia instantánea es 150ppm", 150.0, resultado, 0.05)
    }

    @Test
    fun `CUANDO se detectan setenta pasos en treinta segundos ENTONCES la cadencia instantanea es 140`() {

        calculadora.nuevaMedicionRecibida(1, 1)

        val treintaSegundosEnNanosegundos = 30000000000L
        val resultado = calculadora.nuevaMedicionRecibida(71, treintaSegundosEnNanosegundos + 1)

        Assert.assertEquals("La cadencia instantánea es 140ppm", 140.0, resultado, 0.05)
    }

    @Test
    fun `CUANDO se detectan cuarenta pasos en veinte segundos ENTONCES la cadencia instantanea es 120`() {

        calculadora.nuevaMedicionRecibida(0, 1)

        val veinteSegundosEnNanosegundos = 20000000000L
        val resultado = calculadora.nuevaMedicionRecibida(40, veinteSegundosEnNanosegundos + 1)

        Assert.assertEquals("La cadencia instantánea es 120ppm", 120.0, resultado, 0.05)
    }
}
