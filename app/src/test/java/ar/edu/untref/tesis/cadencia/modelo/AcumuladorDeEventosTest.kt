package ar.edu.untref.tesis.cadencia.modelo

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class AcumuladorDeEventosTest {

    private lateinit var acumulador: AcumuladorDeEventos

    @Before
    fun setup() {

        val tiempoMinimo = 10000000000L // 10 segundos
        val ventanaDeTiempo = 6000000000L // 6 segundos

        acumulador = AcumuladorDeEventos(tiempoMinimo, ventanaDeTiempo)
    }

    @Test
    fun `CUANDO se acumula el primer Evento ENTONCES el acumulador no está listo para calcular la cadencia`() {

        val primerEvento = Evento(1, 1)

        val estaListo = acumulador.acumular(primerEvento)

        Assert.assertFalse(estaListo)
    }

    @Test
    fun `CUANDO se acumulan dos Eventos que no cumplen con el tiempoMinimo requerido ENTONCES el acumulador no está listo para calcular la cadencia`() {

        val primerEvento = Evento(1, 1)
        val segundoEvento = Evento(1, 1000000000) // 9.99 segundos después

        acumulador.acumular(primerEvento)
        val estaListo = acumulador.acumular(segundoEvento)

        Assert.assertFalse(estaListo)
    }

    @Test
    fun `CUANDO se acumulan dos Eventos que cumplen con el tiempoMinimo requerido ENTONCES el acumulador está listo para calcular la cadencia`() {

        val primerEvento = Evento(1, 1)
        val segundoEvento = Evento(1, 10000000001) // 10 segundos después

        acumulador.acumular(primerEvento)
        val estaListo = acumulador.acumular(segundoEvento)

        Assert.assertTrue(estaListo)
    }

    @Test
    fun `CUANDO se acumulan 20 pasos en 10 segundos ENTONCES el acumulador retorna un Evento con 20 pasos y una marca de tiempo de 10000000000 nanosegundos`() {

        val primerEvento = Evento(1, 1)
        val segundoEvento = Evento(21, 10000000001) // 10 segundos después

        acumulador.acumular(primerEvento)
        val estaListo = acumulador.acumular(segundoEvento)
        Assert.assertTrue(estaListo)

        val eventoGenerado = acumulador.obtenerEventoYDeslizarVentana()

        Assert.assertEquals("Se detectaron 20 pasos", 20, eventoGenerado.pasos)
        Assert.assertEquals("El evento tuvo una duración de 10 segundos", 10000000000, eventoGenerado.marcaDeTiempo)
    }

    @Test
    fun `CUANDO desliza la ventana ENTONCES el acumulador no está listo para volver a calcular la cadencia`() {

        val primerEvento = Evento(1, 1)
        val segundoEvento = Evento(21, 10000000001) // 10 segundos después

        acumulador.acumular(primerEvento)
        val estaListo = acumulador.acumular(segundoEvento)
        Assert.assertTrue(estaListo)

        acumulador.obtenerEventoYDeslizarVentana()

        val tercerEvento = Evento(22, 20000000000) // 9.99 segundos después
        val listoOtraVez = acumulador.acumular(tercerEvento)

        Assert.assertFalse(listoOtraVez)
    }
}
