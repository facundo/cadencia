package ar.edu.untref.tesis.cadencia.persistencia.entidades

import ar.edu.untref.tesis.cadencia.PruebaBasica
import ar.edu.untref.tesis.cadencia.R
import org.junit.Assert.* // ktlint-disable no-wildcard-imports
import org.junit.Test

class TipoMedicionTest : PruebaBasica() {

    @Test
    fun `CUANDO se piden los Tipos por su nombre SE DEBE obtener el tipo correcto`() {
        assertEquals(Medicion.Tipo.RITMO, Medicion.Tipo.obtenerPorNombre("RITMO"))
        assertEquals(Medicion.Tipo.ALTIMETRIA, Medicion.Tipo.obtenerPorNombre("ALTIMETRIA"))
        assertEquals(Medicion.Tipo.CADENCIA, Medicion.Tipo.obtenerPorNombre("CADENCIA"))
    }

    @Test
    fun `CUANDO se pide el ícono de cada Tipo SE DEBE obtener el correcto`() {
        assertEquals(R.drawable.icono_ritmo, Medicion.Tipo.RITMO.icono)
        assertEquals(R.drawable.icono_altimetria, Medicion.Tipo.ALTIMETRIA.icono)
        assertEquals(R.drawable.icono_cadencia, Medicion.Tipo.CADENCIA.icono)
    }

    @Test
    fun `CUANDO se pide la unidad de cada Tipo SE DEBE obtener la correcta`() {
        assertEquals("min/km", Medicion.Tipo.RITMO.unidad)
        assertEquals("mts", Medicion.Tipo.ALTIMETRIA.unidad)
        assertEquals("ppm", Medicion.Tipo.CADENCIA.unidad)
    }
}
