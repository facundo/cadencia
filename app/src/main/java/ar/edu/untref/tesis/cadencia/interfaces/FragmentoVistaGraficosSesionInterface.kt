package ar.edu.untref.tesis.cadencia.interfaces

import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion

interface FragmentoVistaGraficosSesionInterface {

    fun dibujar(
        @StringRes titulo: Int,
        mediciones: List<Medicion>,
        @ColorInt color: Int,
        fechaPrimerMedicion: Long,
        invertirEjeY: Boolean,
        formateadorDeValores: (Long) -> String = { it.toString() }
    )
}
