package ar.edu.untref.tesis.cadencia.modelo

class CalculadoraDeCadencia {

    private val acumulador = AcumuladorDeEventos(tiempoMinimoParaCalcularCadencia = 10000000000,
                                                ventanaDeTiempo = 6000000000)

    private val unMinuto = 60000000000.0

    fun nuevaMedicionRecibida(cantidadDePasos: Int, marcaDeTiempo: Long): Double {

        val listoParaCalcularCadencia = acumulador.acumular(Evento(cantidadDePasos, marcaDeTiempo))

        return if (listoParaCalcularCadencia) {
            val acumulado = acumulador.obtenerEventoYDeslizarVentana()
            calcularCadenciaInstantanea(acumulado.pasos, acumulado.marcaDeTiempo)
        } else {
            0.0
        }
    }

    private fun calcularCadenciaInstantanea(nuevosPasosDetectados: Int, tiempoTranscurrido: Long) =
            unMinuto * nuevosPasosDetectados / tiempoTranscurrido
}
