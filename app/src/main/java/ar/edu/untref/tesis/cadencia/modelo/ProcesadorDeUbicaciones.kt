package ar.edu.untref.tesis.cadencia.modelo

import ar.edu.untref.tesis.cadencia.persistencia.entidades.DatosAdicionales
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import com.google.gson.Gson

object ProcesadorDeUbicaciones {

    private val gson = Gson()

    fun crearMedicionDeRitmo(ubicacion: Ubicacion, sesionId: Long) = if (ubicacion.location.hasSpeed()) {

        val segundosPorKilometro = 1000 / ubicacion.location.speed.toDouble()

        Medicion(
                sesion = sesionId,
                tipo = Medicion.Tipo.RITMO.name,
                fecha = ubicacion.location.elapsedRealtimeNanos,
                valor = segundosPorKilometro,
                extra = DatosAdicionales(
                        latitud = ubicacion.location.latitude,
                        longitud = ubicacion.location.longitude,
                        precision = ubicacion.location.speedAccuracyMetersPerSecond,
                        extra = gson.toJson(ubicacion.location),
                        satelitesUtilizados = ubicacion.satelitesUtilizados
                )
        )
    } else {
        null
    }
}
