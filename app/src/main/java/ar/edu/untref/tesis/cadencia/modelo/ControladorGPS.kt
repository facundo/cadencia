@file:Suppress("DEPRECATION")

package ar.edu.untref.tesis.cadencia.modelo

import android.annotation.SuppressLint
import android.location.* // ktlint-disable no-wildcard-imports
import android.os.Bundle

@SuppressLint("MissingPermission")
class ControladorGPS(
    private val locationManager: LocationManager,
    private val listener: Listener
) : LocationListener, GpsStatus.Listener {

    private var satelitesUtilizados = 0

    fun iniciar() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIEMPO_MINIMO_ENTRE_MEDICIONES, DISTANCIA_MINIMA_ENTRE_MEDICIONES, this)
        locationManager.addGpsStatusListener(this)
    }

    fun parar() {
        locationManager.removeGpsStatusListener(this)
        locationManager.removeUpdates(this)
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            listener.ubicacionRecibida(Ubicacion(location, satelitesUtilizados))
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        if (provider.equals(PROVEEDOR_GPS, ignoreCase = true)) {
            if (status == LocationProvider.OUT_OF_SERVICE || status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
                satelitesUtilizados = 0
            }
        }
    }

    override fun onProviderEnabled(provider: String?) {
        satelitesUtilizados = 0
    }

    override fun onProviderDisabled(provider: String?) {
        satelitesUtilizados = 0
    }

    override fun onGpsStatusChanged(event: Int) {
        val gpsStatus = locationManager.getGpsStatus(null)
        satelitesUtilizados = gpsStatus.satellites.count { it.usedInFix() }

        if (satelitesUtilizados >= FiltroDeMedicionesDeRitmo.CANTIDAD_MINIMA_DE_SATELITES_UTILIZADOS) {
            listener.gpsListo()
        }
    }

    companion object {
        private const val PROVEEDOR_GPS = "gps"
        private const val TIEMPO_MINIMO_ENTRE_MEDICIONES = 0L
        private const val DISTANCIA_MINIMA_ENTRE_MEDICIONES = 0f
    }

    interface Listener {
        fun gpsListo()
        fun ubicacionRecibida(ubicacion: Ubicacion)
    }
}
