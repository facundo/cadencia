package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.interfaces.ActividadDetalleSesionInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.persistencia.Repositorio
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetalleSesionPresenter(
    private val vista: ActividadDetalleSesionInterface,
    private val sesionId: Long,
    private val repositorio: Repositorio = Repositorio()
) {

    fun iniciar(dispatcher: CoroutineDispatcher = Dispatchers.Main) {
        GlobalScope.launch(dispatcher) {
            medicionesObtenidas(repositorio.obtenerMediciones(sesionId))
        }
    }

    private fun medicionesObtenidas(mediciones: List<Medicion>) {
        val detalle = DetalleSesion(mediciones)
        vista.prepararSecciones(detalle)
    }
}
