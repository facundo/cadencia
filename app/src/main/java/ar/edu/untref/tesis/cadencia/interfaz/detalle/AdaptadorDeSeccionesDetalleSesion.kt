package ar.edu.untref.tesis.cadencia.interfaz.detalle

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import ar.edu.untref.tesis.cadencia.interfaz.detalle.grafico.FragmentoVistaGraficosSesion
import ar.edu.untref.tesis.cadencia.interfaz.detalle.listado.FragmentoVistaListadoMediciones
import ar.edu.untref.tesis.cadencia.interfaz.detalle.resumen.FragmentoVistaResumen
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion

class AdaptadorDeSeccionesDetalleSesion(val detalleSesion: DetalleSesion, fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val resumen by lazy { FragmentoVistaResumen.newInstance(detalleSesion) }
    private val listado by lazy { FragmentoVistaListadoMediciones.newInstance(detalleSesion) }
    private val graficos by lazy { FragmentoVistaGraficosSesion.newInstance(detalleSesion) }

    override fun getItem(position: Int) = when (position) {
        0 -> resumen
        1 -> listado
        else -> graficos
    }

    override fun getCount() = CANTIDAD_TABS

    override fun getPageTitle(position: Int) = when (position) {
        0 -> "Resumen"
        1 -> "Listado"
        else -> "Gráficos"
    }

    companion object {
        private const val CANTIDAD_TABS = 3
    }
}
