package ar.edu.untref.tesis.cadencia.util

import android.text.format.DateUtils
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion

object FormateadorDeTiempo {

    fun tiempoTranscurrido(medicion: Medicion, detalle: DetalleSesion) = tiempoTranscurrido(medicion, detalle.mediciones.first().fecha)

    fun tiempoTranscurrido(medicion: Medicion, fechaPrimerMedicion: Long): String {
        val segundosTranscurridos = (medicion.fecha - fechaPrimerMedicion) / 1000000000L
        return enMinutos(segundosTranscurridos)
    }

    fun enMinutos(segundos: Long) = DateUtils.formatElapsedTime(segundos)
}
