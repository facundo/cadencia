package ar.edu.untref.tesis.cadencia.modelo

import android.location.Location

data class Ubicacion(val location: Location, val satelitesUtilizados: Int)
