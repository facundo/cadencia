package ar.edu.untref.tesis.cadencia.interfaces

interface ActividadSesionActivaInterface {

    fun prepararSensores()
    fun mostrarLayoutSesionActiva()
    fun actualizarCadencia(cadenciaInstantea: String)
    fun tienePermisosParaAccederAUbicacion(): Boolean
    fun empezarAPedirPosicionesAlGPS()
    fun pedirPermisosParaAccederAUbicacion()
    fun mostrarMensajeNoHayPermisosParaAccederAUbicacion()
    fun ocultarMensajeEsperandoUbicaciones()
}
