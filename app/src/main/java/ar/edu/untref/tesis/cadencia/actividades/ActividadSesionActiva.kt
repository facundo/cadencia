package ar.edu.untref.tesis.cadencia.actividades

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.* // ktlint-disable no-wildcard-imports
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.interfaces.ActividadSesionActivaInterface
import ar.edu.untref.tesis.cadencia.modelo.ControladorGPS
import ar.edu.untref.tesis.cadencia.modelo.Ubicacion
import ar.edu.untref.tesis.cadencia.presenters.SesionActivaPresenter
import kotlinx.android.synthetic.main.actividad_sesion_activa.*

class ActividadSesionActiva : ActividadBasica(), ActividadSesionActivaInterface, SensorEventListener, ControladorGPS.Listener {

    private lateinit var administradorDeSensores: SensorManager
    private lateinit var contadorDePasos: Sensor
    private lateinit var controladorGps: ControladorGPS
    private lateinit var presenter: SesionActivaPresenter

    override fun getLayout() = R.layout.actividad_sesion_activa

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = SesionActivaPresenter(this)

        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        controladorGps = ControladorGPS(lm, this)

        presenter.iniciar()

        botonIniciar.setOnClickListener {
            presenter.iniciarSesion()
        }

        botonFinalizar.setOnClickListener {
            onBackPressed()
        }
    }

    override fun tienePermisosParaAccederAUbicacion() =
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    @SuppressLint("MissingPermission")
    override fun empezarAPedirPosicionesAlGPS() {

        esperandoUbicaciones.visibility = View.VISIBLE
        controladorGps.iniciar()
    }

    override fun pedirPermisosParaAccederAUbicacion() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_CODE
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        presenter.respuestaDelUsuarioAlPedidoDePermisos(
                requestCode == REQUEST_LOCATION_CODE &&
                        grantResults.first() == PackageManager.PERMISSION_GRANTED
        )
    }

    override fun mostrarMensajeNoHayPermisosParaAccederAUbicacion() {
        noHayGPS.visibility = View.VISIBLE
    }

    override fun ocultarMensajeEsperandoUbicaciones() {
        esperandoUbicaciones.visibility = View.GONE
    }

    override fun gpsListo() {
        if (!presenter.estaActiva) {
            gpsListo.visibility = View.VISIBLE
            esperandoUbicaciones.visibility = View.GONE
        }
    }

    override fun ubicacionRecibida(location: Ubicacion) {
        presenter.ubicacionActualizada(location)
    }

    override fun prepararSensores() {
        administradorDeSensores = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        gpsListo.visibility = View.GONE
        noHayGPS.visibility = View.GONE
        contadorDePasos = administradorDeSensores.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        administradorDeSensores.registerListener(this, contadorDePasos, SensorManager.SENSOR_DELAY_FASTEST)
    }

    override fun mostrarLayoutSesionActiva() {
        gpsListo.visibility = View.GONE
        noHayGPS.visibility = View.GONE
        esperandoUbicaciones.visibility = View.GONE
        layoutInicio.visibility = View.GONE
        layoutSesion.visibility = View.VISIBLE
    }

    /* Callback innecesaria para usar el STEP COUNTER */
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {

        event?.let {

            val sensor = event.sensor

            if (sensor?.type == Sensor.TYPE_STEP_COUNTER) {

                val cantidad = event.values[0].toInt()
                val marcaDeTiempo = event.timestamp

                Log.d(ActividadSesionActiva::class.java.name, "Pasos actualizados: cantidad: $cantidad, tiempo: $marcaDeTiempo")
                presenter.cantidadDePasosActualizada(cantidad, marcaDeTiempo)
            }
        }
    }

    override fun actualizarCadencia(cadenciaInstantea: String) {
        valorCadencia.text = cadenciaInstantea
    }

    override fun onStop() {
        super.onStop()
        if (this::administradorDeSensores.isInitialized) {
            administradorDeSensores.unregisterListener(this, contadorDePasos)
        }
        controladorGps.parar()
    }

    companion object {
        private const val REQUEST_LOCATION_CODE = 1
    }
}
