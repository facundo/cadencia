package ar.edu.untref.tesis.cadencia.interfaces

import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion

interface ActividadDetalleSesionInterface {
    fun prepararSecciones(detalleSesion: DetalleSesion)
}
