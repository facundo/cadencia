package ar.edu.untref.tesis.cadencia.modelo

import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import java.io.Serializable

data class DetalleSesion(val mediciones: List<Medicion>) : Serializable
