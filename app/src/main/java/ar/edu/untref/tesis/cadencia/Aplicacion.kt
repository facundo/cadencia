package ar.edu.untref.tesis.cadencia

import android.app.Application
import com.google.firebase.crashlytics.FirebaseCrashlytics

class Aplicacion : Application() {

    override fun onCreate() {
        super.onCreate()
        instancia = this
        configurarCrashlytics()
    }

    /**
     * Configura Crashlytics, pero lo deshabilita en modo DEBUG para no registrar
     * en la consola los crashes ocurridos en la etapa de desarollo.
     */
    private fun configurarCrashlytics() {
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG)
    }

    companion object {
        lateinit var instancia: Aplicacion
            private set
    }
}
