package ar.edu.untref.tesis.cadencia.interfaz.detalle

import android.os.Bundle
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.actividades.ActividadBasica
import ar.edu.untref.tesis.cadencia.interfaces.ActividadDetalleSesionInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.presenters.DetalleSesionPresenter
import kotlinx.android.synthetic.main.actividad_detalle_sesion.*

class ActividadDetalleSesion : ActividadBasica(), ActividadDetalleSesionInterface {

    private lateinit var presenter: DetalleSesionPresenter

    override fun getLayout() = R.layout.actividad_detalle_sesion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sesionId = intent.getLongExtra(SESION_ID, 0)
        presenter = DetalleSesionPresenter(this, sesionId)

        supportActionBar?.title = "Detalle de sesión #$sesionId"

        tabLayout.setupWithViewPager(pager)

        presenter.iniciar()
    }

    override fun prepararSecciones(detalleSesion: DetalleSesion) {
        pager.adapter = AdaptadorDeSeccionesDetalleSesion(detalleSesion, supportFragmentManager)
    }

    companion object {
        const val SESION_ID = "sesion_id"
    }
}
