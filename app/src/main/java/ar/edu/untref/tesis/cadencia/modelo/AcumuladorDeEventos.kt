package ar.edu.untref.tesis.cadencia.modelo

/**
 * pre: tiempoMinimoParaCalcularCadencia y ventanaDeTiempo deben expresarse en nanosegundos.
 */
class AcumuladorDeEventos(
    private val tiempoMinimoParaCalcularCadencia: Long,
    private val ventanaDeTiempo: Long
) {

    private var eventos = arrayListOf<Evento>()

    /**
     * pre: evento contiene la cantidad de pasos contados y la marca de tiempo en la que
     * se detectó el último de esos pasos, en nanosegundos.
     * post: indica si ya se han acumulado suficientes Eventos para calcular la cadencia instantánea.
     */
    fun acumular(evento: Evento): Boolean {

        eventos.add(evento)

        val primerEvento = eventos.first()
        val ultimoEvento = eventos.last()

        return ultimoEvento.marcaDeTiempo - primerEvento.marcaDeTiempo >= tiempoMinimoParaCalcularCadencia
    }

    /**
     * pre: debe ser llamado únicamente cuando el Acumulador lo haya indicado al invocar nuevoEvento.
     * post: Retorna un Evento que contiene la cantidad de pasos detectada
     * durante la acumulación y el tiempo que esta duró.
     */
    fun obtenerEventoYDeslizarVentana(): Evento {

        val primerEvento = eventos[0]
        val ultimoEvento = eventos.last()

        val evento = Evento(ultimoEvento.pasos - primerEvento.pasos,
                ultimoEvento.marcaDeTiempo - primerEvento.marcaDeTiempo)

        eventos = eventos.filter {
            ultimoEvento.marcaDeTiempo - it.marcaDeTiempo < ventanaDeTiempo
        } as ArrayList<Evento>

        return evento
    }
}
