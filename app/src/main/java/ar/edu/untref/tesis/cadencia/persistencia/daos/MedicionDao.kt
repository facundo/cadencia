package ar.edu.untref.tesis.cadencia.persistencia.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion

@Dao
interface MedicionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun guardar(medicion: Medicion)

    @Query("SELECT * from medicion where sesion = :idSesion ORDER BY fecha ASC")
    suspend fun obtenerMediciones(idSesion: Long): List<Medicion>

    @Query("SELECT COUNT(1) from medicion where sesion = :idSesion and tipo = :tipo")
    suspend fun cantidadDeMediciones(idSesion: Long, tipo: String): Int
}
