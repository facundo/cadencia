package ar.edu.untref.tesis.cadencia.persistencia

import androidx.room.Room
import ar.edu.untref.tesis.cadencia.Aplicacion
import ar.edu.untref.tesis.cadencia.persistencia.daos.MedicionDao
import ar.edu.untref.tesis.cadencia.persistencia.daos.SesionDao
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion

class Repositorio {

    private val BASE_DE_DATOS = "cadencia-db"

    private val medicionDao: MedicionDao
    private val sesionDao: SesionDao

    init {

        val bbdd = Room.databaseBuilder(Aplicacion.instancia,
                BaseDeDatos::class.java,
                BASE_DE_DATOS)
                .fallbackToDestructiveMigration()
                .build()

        this.medicionDao = bbdd.medicionDao()
        this.sesionDao = bbdd.sesionDao()
    }

    suspend fun nuevaSesion(): Sesion {

        val nuevaSesion = Sesion()
        val id = sesionDao.guardar(nuevaSesion)
        nuevaSesion.id = id

        return nuevaSesion
    }

    suspend fun obtenerSesiones(): List<Sesion> {

        val sesiones = sesionDao.obtenerSesiones()

        sesiones.forEach {
            it.tieneCadencia = tieneMediciones(it, Medicion.Tipo.CADENCIA)
            it.tieneRitmo = tieneMediciones(it, Medicion.Tipo.RITMO)
        }

        return sesiones
    }

    private suspend fun tieneMediciones(sesion: Sesion, tipo: Medicion.Tipo) = medicionDao.cantidadDeMediciones(sesion.id, tipo.name) > 0

    suspend fun obtenerMediciones(idSesion: Long): List<Medicion> {
        return medicionDao.obtenerMediciones(idSesion)
    }

    suspend fun guardar(medicion: Medicion) {
        medicionDao.guardar(medicion)
    }
}
