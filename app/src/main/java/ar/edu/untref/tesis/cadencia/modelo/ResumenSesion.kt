package ar.edu.untref.tesis.cadencia.modelo

data class ResumenSesion(
    val duracion: String,
    val cadenciaMaxima: String,
    val cadenciaMinima: String,
    val cadenciaPromedio: String,
    val ritmoMaximo: String,
    val ritmoMinimo: String,
    val ritmoPromedio: String
)
