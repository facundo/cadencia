package ar.edu.untref.tesis.cadencia.interfaces

interface ActividadSesionesInterface {

    fun cargando(estaCargando: Boolean)
    fun mostrarMensajeNoHaySesiones()
    fun mostrarSesiones()
    fun irADetalleSesion(sesionId: Long)
}
