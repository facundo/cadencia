package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.interfaces.ActividadSesionesInterface
import ar.edu.untref.tesis.cadencia.persistencia.Repositorio
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SesionesPresenter(
    private val vista: ActividadSesionesInterface,
    private val repositorio: Repositorio = Repositorio()
) {

    var sesiones = emptyList<Sesion>()

    fun iniciar(dispatcher: CoroutineDispatcher = Dispatchers.Main) {

        vista.cargando(true)

        GlobalScope.launch(dispatcher) {
            sesiones = repositorio.obtenerSesiones()
            sesionesObtenidas(sesiones)
        }
    }

    fun sesionesObtenidas(sesiones: List<Sesion>) {

        this.sesiones = sesiones
        vista.cargando(false)

        if (sesiones.isEmpty()) {
            vista.mostrarMensajeNoHaySesiones()
        } else {
            vista.mostrarSesiones()
        }
    }

    fun sesionClickeada(posicion: Int) {
        vista.irADetalleSesion(sesiones[posicion].id)
    }
}
