package ar.edu.untref.tesis.cadencia.persistencia.entidades

import androidx.annotation.DrawableRes
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import ar.edu.untref.tesis.cadencia.R
import java.io.Serializable

@Entity
data class Medicion(
    var valor: Double = 0.0,
    var sesion: Long = 0L,
    var tipo: String = "",
    var fecha: Long = 0,
    @Embedded var extra: DatosAdicionales? = null
) : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    enum class Tipo(@DrawableRes val icono: Int, val unidad: String) {
        CADENCIA(R.drawable.icono_cadencia, "ppm"),
        RITMO(R.drawable.icono_ritmo, "min/km"),
        ALTIMETRIA(R.drawable.icono_altimetria, "mts");

        companion object {
            fun obtenerPorNombre(nombre: String) = values().filter { it.name == nombre }.first()
        }
    }
}
