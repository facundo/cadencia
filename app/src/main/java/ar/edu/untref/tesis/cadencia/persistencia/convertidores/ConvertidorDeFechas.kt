package ar.edu.untref.tesis.cadencia.persistencia.convertidores

import androidx.room.TypeConverter
import java.util.Date

class ConvertidorDeFechas {

    @TypeConverter
    fun desdeFecha(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun convertirFecha(date: Date?): Long? {
        return date?.time?.toLong()
    }
}
