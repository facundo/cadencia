package ar.edu.untref.tesis.cadencia.persistencia

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ar.edu.untref.tesis.cadencia.persistencia.convertidores.ConvertidorDeFechas
import ar.edu.untref.tesis.cadencia.persistencia.daos.MedicionDao
import ar.edu.untref.tesis.cadencia.persistencia.daos.SesionDao
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion

@Database(version = 10, entities = [Sesion::class, Medicion::class])
@TypeConverters(ConvertidorDeFechas::class)
abstract class BaseDeDatos : RoomDatabase() {
    abstract fun medicionDao(): MedicionDao
    abstract fun sesionDao(): SesionDao
}
