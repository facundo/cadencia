package ar.edu.untref.tesis.cadencia.modelo

data class Evento(val pasos: Int, val marcaDeTiempo: Long)
