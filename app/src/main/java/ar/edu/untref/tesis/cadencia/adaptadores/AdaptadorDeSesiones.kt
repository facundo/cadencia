package ar.edu.untref.tesis.cadencia.adaptadores

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.presenters.SesionesPresenter
import java.text.DateFormat
import kotlinx.android.synthetic.main.item_sesion.view.*

class AdaptadorDeSesiones(private val presenter: SesionesPresenter) : RecyclerView.Adapter<HolderBasico>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderBasico {
        val itemLayoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_sesion, null)
        return HolderBasico(itemLayoutView)
    }

    override fun getItemCount() = presenter.sesiones.size

    override fun onBindViewHolder(holder: HolderBasico, position: Int) {

        val sesion = presenter.sesiones[position]

        holder.itemView.titulo.text = "Sesión ${sesion.id}"
        holder.itemView.fecha.text = "${DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(sesion.fecha)} hs"
        holder.itemView.iconoCadencia.isVisible = sesion.tieneCadencia
        holder.itemView.iconoRitmo.isVisible = sesion.tieneRitmo

        holder.itemView.setOnClickListener {
            presenter.sesionClickeada(position)
        }
    }

    fun mostrarSesiones() {
        notifyDataSetChanged()
    }
}
