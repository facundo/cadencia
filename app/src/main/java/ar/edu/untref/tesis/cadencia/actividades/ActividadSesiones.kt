package ar.edu.untref.tesis.cadencia.actividades

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.adaptadores.AdaptadorDeSesiones
import ar.edu.untref.tesis.cadencia.interfaces.ActividadSesionesInterface
import ar.edu.untref.tesis.cadencia.interfaz.detalle.ActividadDetalleSesion
import ar.edu.untref.tesis.cadencia.presenters.SesionesPresenter
import kotlinx.android.synthetic.main.actividad_sesiones.*

class ActividadSesiones : ActividadBasica(), ActividadSesionesInterface {

    private lateinit var presenter: SesionesPresenter
    private lateinit var adapter: AdaptadorDeSesiones

    override fun getLayout() = R.layout.actividad_sesiones

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.mis_sesiones)

        presenter = SesionesPresenter(this)

        recycler.layoutManager = LinearLayoutManager(this)
        adapter = AdaptadorDeSesiones(presenter)
        recycler.adapter = adapter

        fab.setOnClickListener {
            val intent = Intent(this, ActividadSesionActiva::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.iniciar()
    }

    @SuppressLint("RestrictedApi")
    override fun cargando(estaCargando: Boolean) {

        val visibilidadProgress = if (estaCargando) View.VISIBLE else View.GONE
        val visibilidadListado = if (estaCargando) View.GONE else View.VISIBLE

        progress.visibility = visibilidadProgress
        fab.visibility = visibilidadListado
    }

    override fun mostrarMensajeNoHaySesiones() {
        recycler.visibility = View.GONE
        estadoVacio.visibility = View.VISIBLE
    }

    override fun mostrarSesiones() {
        adapter.mostrarSesiones()
        recycler.visibility = View.VISIBLE
        estadoVacio.visibility = View.GONE
    }

    override fun irADetalleSesion(sesionId: Long) {
        val intent = Intent(this, ActividadDetalleSesion::class.java)
        intent.putExtra(ActividadDetalleSesion.SESION_ID, sesionId)
        startActivity(intent)
    }
}
