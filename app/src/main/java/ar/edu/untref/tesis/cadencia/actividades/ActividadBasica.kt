package ar.edu.untref.tesis.cadencia.actividades

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class ActividadBasica : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
    }

    abstract fun getLayout(): Int
}
