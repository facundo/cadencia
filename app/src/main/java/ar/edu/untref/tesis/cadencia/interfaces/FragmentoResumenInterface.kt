package ar.edu.untref.tesis.cadencia.interfaces

import ar.edu.untref.tesis.cadencia.modelo.ResumenSesion

interface FragmentoResumenInterface {
    fun mostrarResumen(resumen: ResumenSesion)
}
