package ar.edu.untref.tesis.cadencia.interfaz.detalle.resumen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoResumenInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.modelo.ResumenSesion
import ar.edu.untref.tesis.cadencia.presenters.ResumenPresenter
import kotlinx.android.synthetic.main.fragmento_vista_resumen_sesion.*

class FragmentoVistaResumen : Fragment(R.layout.fragmento_vista_resumen_sesion), FragmentoResumenInterface {

    private lateinit var presenter: ResumenPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val detalleSesion = requireArguments()[PARAMETRO_DETALLE] as DetalleSesion
        presenter = ResumenPresenter(this, detalleSesion)
        presenter.iniciar()
    }

    companion object {
        private const val PARAMETRO_DETALLE = "detalle"

        fun newInstance(detalleSesion: DetalleSesion): FragmentoVistaResumen {

            val fragmento = FragmentoVistaResumen()
            val parametros = Bundle()
            parametros.putSerializable(PARAMETRO_DETALLE, detalleSesion)
            fragmento.arguments = parametros

            return fragmento
        }
    }

    override fun mostrarResumen(resumen: ResumenSesion) {
        duracion.text = resumen.duracion

        cadenciaMinima.text = resumen.cadenciaMinima
        cadenciaMaxima.text = resumen.cadenciaMaxima
        cadenciaPromedio.text = resumen.cadenciaPromedio

        ritmoMinimo.text = resumen.ritmoMinimo
        ritmoMaximo.text = resumen.ritmoMaximo
        ritmoPromedio.text = resumen.ritmoPromedio
    }
}
