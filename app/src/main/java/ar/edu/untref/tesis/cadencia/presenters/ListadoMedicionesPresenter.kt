package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.interfaces.FragmentoListadoMedicionesInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion

class ListadoMedicionesPresenter(
    private val vista: FragmentoListadoMedicionesInterface,
    val detalleSesion: DetalleSesion
) {

    fun iniciar() {
        vista.mostrarMediciones()
    }
}
