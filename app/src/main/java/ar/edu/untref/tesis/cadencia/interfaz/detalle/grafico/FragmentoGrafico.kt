package ar.edu.untref.tesis.cadencia.interfaz.detalle.grafico

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.util.FormateadorDeTiempo
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import java.io.Serializable
import kotlinx.android.synthetic.main.fragmento_grafico.*

class FragmentoGrafico : Fragment(R.layout.fragmento_grafico) {

    private val formateador: FormateadorDeTiempo = FormateadorDeTiempo
    private lateinit var lineData: LineData

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val parametros = requireArguments()[PARAMETRO] as Parametros
        configurarGrafico(parametros.titulo, parametros.fechaPrimerMedicion, parametros.invertirEjeY, parametros.formateadorDeValores)
        dibujar(parametros)
    }

    private fun configurarGrafico(@StringRes tituloStringRes: Int, fechaPrimerMedicion: Long, invertirEjeY: Boolean, formateadorDeValores: (Long) -> String) {

        titulo.text = getString(tituloStringRes)

        grafico.setTouchEnabled(true)
        grafico.setPinchZoom(true)
        grafico.description.isEnabled = false

        val ejeX = grafico.xAxis
        ejeX.position = XAxis.XAxisPosition.BOTTOM
        ejeX.setDrawGridLines(false)
        ejeX.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return formateador.tiempoTranscurrido(Medicion(fecha = value.toLong()), fechaPrimerMedicion)
            }
        }
        grafico.axisRight.isEnabled = false

        val ejeY = grafico.axisLeft
        ejeY.isInverted = invertirEjeY
        ejeY.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return return formateadorDeValores(value.toLong())
            }
        }
    }

    private fun dibujar(parametros: Parametros) {

        val entries = mutableListOf<Entry>()

        for (medicion in parametros.mediciones) {

            val entry = Entry()
            entry.x = medicion.fecha.toFloat()
            entry.y = medicion.valor.toFloat()

            entries.add(entry)
        }

        val dataSet = LineDataSet(entries, getString(parametros.titulo))

        dataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return parametros.formateadorDeValores(value.toLong())
            }
        }

        dataSet.color = parametros.color

        lineData = LineData(dataSet)

        grafico.data = lineData
        grafico.invalidate()
    }

    companion object {

        private const val PARAMETRO = "parametros"

        fun newInstance(parametros: Parametros) = FragmentoGrafico().apply {
            arguments = bundleOf(PARAMETRO to parametros)
        }
    }

    data class Parametros(
        val titulo: Int,
        val mediciones: List<Medicion>,
        val color: Int,
        val fechaPrimerMedicion: Long,
        val formateadorDeValores: (Long) -> String,
        val invertirEjeY: Boolean
    ) : Serializable
}
