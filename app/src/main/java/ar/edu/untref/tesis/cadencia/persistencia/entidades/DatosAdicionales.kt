package ar.edu.untref.tesis.cadencia.persistencia.entidades

data class DatosAdicionales(
    val latitud: Double,
    val longitud: Double,
    val precision: Float,
    val extra: String,
    val satelitesUtilizados: Int
)
