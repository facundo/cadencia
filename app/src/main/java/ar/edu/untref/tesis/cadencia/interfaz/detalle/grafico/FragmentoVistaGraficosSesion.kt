package ar.edu.untref.tesis.cadencia.interfaz.detalle.grafico

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoVistaGraficosSesionInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.presenters.VistaGraficosSesionPresenter
import kotlinx.android.synthetic.main.fragmento_vista_graficos_detalle.*

class FragmentoVistaGraficosSesion : Fragment(R.layout.fragmento_vista_graficos_detalle), FragmentoVistaGraficosSesionInterface {

    private lateinit var presenter: VistaGraficosSesionPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val detalle = requireArguments()[PARAMETRO_DETALLE] as DetalleSesion
        presenter = VistaGraficosSesionPresenter(this, detalle)
        presenter.prepararGrafico()
    }

    companion object {

        private const val PARAMETRO_DETALLE = "detalle"

        fun newInstance(detalle: DetalleSesion): FragmentoVistaGraficosSesion {

            val fragmento = FragmentoVistaGraficosSesion()
            val parametros = Bundle()
            parametros.putSerializable(PARAMETRO_DETALLE, detalle)
            fragmento.arguments = parametros

            return fragmento
        }
    }

    override fun dibujar(
        titulo: Int,
        mediciones: List<Medicion>,
        color: Int,
        fechaPrimerMedicion: Long,
        invertirEjeY: Boolean,
        formateadorDeValores: (Long) -> String
    ) {
        val fragmentContainerView = FragmentContainerView(requireContext()).apply {
            id = View.generateViewId()
        }

        contenido.addView(fragmentContainerView)

        val parametros = FragmentoGrafico.Parametros(titulo, mediciones, color, fechaPrimerMedicion, formateadorDeValores, invertirEjeY)
        childFragmentManager.beginTransaction()
                .replace(fragmentContainerView.id, FragmentoGrafico.newInstance(parametros))
                .commitNow()
    }
}
