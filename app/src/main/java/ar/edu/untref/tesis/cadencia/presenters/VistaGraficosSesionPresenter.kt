package ar.edu.untref.tesis.cadencia.presenters

import android.graphics.Color
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoVistaGraficosSesionInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.modelo.FiltroDeMedicionesDeRitmo
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.util.FormateadorDeTiempo

class VistaGraficosSesionPresenter(
    private val vista: FragmentoVistaGraficosSesionInterface,
    private val detalle: DetalleSesion,
    private val filtro: FiltroDeMedicionesDeRitmo = FiltroDeMedicionesDeRitmo()
) {

    fun prepararGrafico() {

        vista.dibujar(
                titulo = R.string.etiqueta_grafico_cadencia,
                mediciones = detalle.mediciones.filter { it.tipo == Medicion.Tipo.CADENCIA.name },
                color = Color.CYAN,
                invertirEjeY = false,
                fechaPrimerMedicion = detalle.mediciones.first().fecha
        )

        vista.dibujar(
                titulo = R.string.etiqueta_grafico_ritmo,
                mediciones = filtro.filtrar(detalle.mediciones.filter { it.tipo == Medicion.Tipo.RITMO.name }),
                color = Color.BLACK,
                fechaPrimerMedicion = detalle.mediciones.first().fecha,
                invertirEjeY = true,
                formateadorDeValores = { valor ->
                    FormateadorDeTiempo.enMinutos(valor)
                })
    }
}
