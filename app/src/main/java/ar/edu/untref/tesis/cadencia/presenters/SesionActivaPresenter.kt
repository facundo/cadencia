package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.interfaces.ActividadSesionActivaInterface
import ar.edu.untref.tesis.cadencia.modelo.CalculadoraDeCadencia
import ar.edu.untref.tesis.cadencia.modelo.ProcesadorDeUbicaciones
import ar.edu.untref.tesis.cadencia.modelo.Ubicacion
import ar.edu.untref.tesis.cadencia.persistencia.Repositorio
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

class SesionActivaPresenter(
    private val vista: ActividadSesionActivaInterface,
    private val repositorio: Repositorio = Repositorio(),
    private val calculadoraDeCadencia: CalculadoraDeCadencia = CalculadoraDeCadencia()
) {

    var estaActiva = false
    private var sesionActiva: Sesion? = null

    fun iniciar() {
        if (vista.tienePermisosParaAccederAUbicacion()) {
            vista.empezarAPedirPosicionesAlGPS()
        } else {
            vista.pedirPermisosParaAccederAUbicacion()
        }
    }

    fun respuestaDelUsuarioAlPedidoDePermisos(aceptoElPedido: Boolean) {
        if (aceptoElPedido) {
            vista.empezarAPedirPosicionesAlGPS()
        } else {
            vista.mostrarMensajeNoHayPermisosParaAccederAUbicacion()
        }
    }

    fun iniciarSesion() {
        estaActiva = true
        vista.mostrarLayoutSesionActiva()
        vista.prepararSensores()
    }

    /**
     * pre: cantidad es la cantidad de pasos desde el último reinicio del teéfono.
     * pre: marcaDeTiempo es la hora exacta en la que sucedió el evento, medida en nanosegundos.
     */
    fun cantidadDePasosActualizada(cantidad: Int, marcaDeTiempo: Long, dispatcher: CoroutineDispatcher = Dispatchers.IO) {
        val cadenciaInstantanea = calculadoraDeCadencia.nuevaMedicionRecibida(cantidad, marcaDeTiempo)

        if (cadenciaInstantanea != 0.0) {
            guardarMedicionDeCadencia(cadenciaInstantanea, marcaDeTiempo, dispatcher)
            vista.actualizarCadencia("${cadenciaInstantanea.toLong()} ppm")
        }
    }

    private fun guardarMedicionDeCadencia(cadenciaInstantanea: Double, marcaDeTiempo: Long, dispatcher: CoroutineDispatcher) {
        Medicion(
                tipo = Medicion.Tipo.CADENCIA.name,
                fecha = marcaDeTiempo,
                valor = cadenciaInstantanea
        ).let {
            GlobalScope.launch(dispatcher) {
                it.sesion = getSesionId()
                repositorio.guardar(it)
            }
        }
    }

    fun ubicacionActualizada(ubicacion: Ubicacion, dispatcher: CoroutineDispatcher = Dispatchers.IO) {
        vista.ocultarMensajeEsperandoUbicaciones()

        if (estaActiva) {
            GlobalScope.launch(dispatcher) {
                ProcesadorDeUbicaciones.crearMedicionDeRitmo(ubicacion, getSesionId())?.let {
                    repositorio.guardar(it)
                }
            }
        }
    }

    private suspend fun getSesionId(): Long {

        sesionActiva = (sesionActiva ?: repositorio.nuevaSesion())

        return sesionActiva!!.id
    }
}
