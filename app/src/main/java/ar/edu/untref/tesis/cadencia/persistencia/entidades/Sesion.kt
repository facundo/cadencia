package ar.edu.untref.tesis.cadencia.persistencia.entidades

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity
data class Sesion(
    @JvmField @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val fecha: Date = Date(),
    var tieneCadencia: Boolean = false,
    var tieneRitmo: Boolean = false
)
