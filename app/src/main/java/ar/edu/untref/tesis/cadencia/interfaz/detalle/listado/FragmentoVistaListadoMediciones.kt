package ar.edu.untref.tesis.cadencia.interfaz.detalle.listado

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.adaptadores.AdaptadorDeRegistros
import ar.edu.untref.tesis.cadencia.interfaces.FragmentoListadoMedicionesInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.presenters.ListadoMedicionesPresenter
import kotlinx.android.synthetic.main.fragmento_vista_listado_mediciones.*

class FragmentoVistaListadoMediciones : Fragment(R.layout.fragmento_vista_listado_mediciones), FragmentoListadoMedicionesInterface {

    private lateinit var presenter: ListadoMedicionesPresenter
    private lateinit var adapter: AdaptadorDeRegistros

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val detalleSesion = requireArguments()[PARAMETRO_DETALLE] as DetalleSesion

        presenter = ListadoMedicionesPresenter(this, detalleSesion)

        adapter = AdaptadorDeRegistros(presenter)
        recycler.adapter = adapter

        presenter.iniciar()
    }

    companion object {
        private const val PARAMETRO_DETALLE = "detalle"

        fun newInstance(detalleSesion: DetalleSesion): FragmentoVistaListadoMediciones {

            val fragmento = FragmentoVistaListadoMediciones()
            val parametros = Bundle()
            parametros.putSerializable(PARAMETRO_DETALLE, detalleSesion)
            fragmento.arguments = parametros

            return fragmento
        }
    }

    override fun mostrarMediciones() {
        adapter.notifyDataSetChanged()
    }
}
