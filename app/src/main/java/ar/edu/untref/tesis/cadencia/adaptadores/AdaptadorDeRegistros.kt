package ar.edu.untref.tesis.cadencia.adaptadores

import android.view.LayoutInflater
import android.view.ViewGroup
import ar.edu.untref.tesis.cadencia.R
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.presenters.ListadoMedicionesPresenter
import ar.edu.untref.tesis.cadencia.util.FormateadorDeTiempo
import kotlinx.android.synthetic.main.item_registro.view.*

class AdaptadorDeRegistros(
    private val presenter: ListadoMedicionesPresenter,
    private val formateador: FormateadorDeTiempo = FormateadorDeTiempo
) : androidx.recyclerview.widget.RecyclerView.Adapter<HolderBasico>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderBasico {
        val itemLayoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_registro, null)
        return HolderBasico(itemLayoutView)
    }

    override fun getItemCount() = presenter.detalleSesion.mediciones.size

    override fun onBindViewHolder(holder: HolderBasico, position: Int) {

        val medicionActual = presenter.detalleSesion.mediciones[position]
        val tipo = Medicion.Tipo.obtenerPorNombre(medicionActual.tipo)

        val valor = when (tipo) {
            Medicion.Tipo.CADENCIA -> medicionActual.valor.toLong()
            Medicion.Tipo.RITMO -> FormateadorDeTiempo.enMinutos(medicionActual.valor.toLong())
            Medicion.Tipo.ALTIMETRIA -> String.format("%.2f", medicionActual.valor)
        }

        holder.itemView.valor.text = "$valor ${tipo.unidad}"
        holder.itemView.minuto.text = formateador.tiempoTranscurrido(medicionActual, presenter.detalleSesion)
        holder.itemView.icono.setImageResource(tipo.icono)
    }
}
