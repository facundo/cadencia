package ar.edu.untref.tesis.cadencia.persistencia.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Sesion

@Dao
interface SesionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun guardar(sesion: Sesion): Long

    @Query("SELECT * from sesion")
    suspend fun obtenerSesiones(): List<Sesion>
}
