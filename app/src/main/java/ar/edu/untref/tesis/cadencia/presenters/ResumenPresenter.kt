package ar.edu.untref.tesis.cadencia.presenters

import ar.edu.untref.tesis.cadencia.interfaces.FragmentoResumenInterface
import ar.edu.untref.tesis.cadencia.modelo.DetalleSesion
import ar.edu.untref.tesis.cadencia.modelo.FiltroDeMedicionesDeRitmo
import ar.edu.untref.tesis.cadencia.modelo.ResumenSesion
import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion
import ar.edu.untref.tesis.cadencia.util.FormateadorDeTiempo

class ResumenPresenter(
    private val vista: FragmentoResumenInterface,
    private val detalleSesion: DetalleSesion,
    private val formateador: FormateadorDeTiempo = FormateadorDeTiempo,
    private val filtro: FiltroDeMedicionesDeRitmo = FiltroDeMedicionesDeRitmo()
) {

    private lateinit var resumen: ResumenSesion

    fun iniciar() {

        val valoresDeCadencia = detalleSesion.mediciones
                .filter { it.tipo == Medicion.Tipo.CADENCIA.name }
                .map { it.valor }

        val cadenciaMaxima = valoresDeCadencia.max()?.toLong() ?: 0
        val cadenciaMinima = valoresDeCadencia.min()?.toLong() ?: 0
        val cadenciaPromedio = valoresDeCadencia.average().let {
            if (it.isNaN()) 0.0 else it
        }

        val segundosTranscurridos = if (detalleSesion.mediciones.isEmpty()) 0 else (detalleSesion.mediciones.last().fecha - detalleSesion.mediciones.first().fecha) / 1000000000L
        val duracion = formateador.enMinutos(segundosTranscurridos)

        val valoresDeRitmo = filtro.filtrar(
                detalleSesion.mediciones
                        .filter { it.tipo == Medicion.Tipo.RITMO.name }
        ).map { it.valor }

        val ritmoMaximo = formateador.enMinutos(valoresDeRitmo.min()?.toLong() ?: 0)
        val ritmoMinimo = formateador.enMinutos(valoresDeRitmo.max()?.toLong() ?: 0)
        val ritmoPromedio = formateador.enMinutos(valoresDeRitmo.average().toLong())

        resumen = ResumenSesion(
                duracion = duracion,
                cadenciaMaxima = "$cadenciaMaxima ${Medicion.Tipo.CADENCIA.unidad}",
                cadenciaMinima = "$cadenciaMinima ${Medicion.Tipo.CADENCIA.unidad}",
                cadenciaPromedio = "${String.format("%.2f", cadenciaPromedio)} ${Medicion.Tipo.CADENCIA.unidad}",
                ritmoMaximo = "$ritmoMaximo ${Medicion.Tipo.RITMO.unidad}",
                ritmoMinimo = "$ritmoMinimo ${Medicion.Tipo.RITMO.unidad}",
                ritmoPromedio = "$ritmoPromedio ${Medicion.Tipo.RITMO.unidad}"
        )

        vista.mostrarResumen(resumen)
    }
}
