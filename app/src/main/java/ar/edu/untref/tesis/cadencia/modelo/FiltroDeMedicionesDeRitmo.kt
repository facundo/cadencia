package ar.edu.untref.tesis.cadencia.modelo

import ar.edu.untref.tesis.cadencia.persistencia.entidades.Medicion

class FiltroDeMedicionesDeRitmo {

    fun filtrar(mediciones: List<Medicion>): List<Medicion> {

        if (mediciones.isEmpty()) {
            return emptyList()
        }

        val lista = listOf(mediciones.first()).toMutableList()
        val medicionesSinLaPrimera = mediciones.toMutableList()
        medicionesSinLaPrimera.removeAt(0)

        lista.addAll(
                medicionesSinLaPrimera.filter {
                    it.extra != null &&
                            it.extra!!.precision <= PRECISION_MINIMA &&
                            it.extra!!.satelitesUtilizados >= CANTIDAD_MINIMA_DE_SATELITES_UTILIZADOS &&
                            it.valor >= RITMO_DE_TRES_MINUTOS_POR_KILOMETRO &&
                            it.valor <= RITMO_DE_OCHO_MINUTOS_POR_KILOMETRO
                }
        )

        for (i in 1 until lista.size) {
            val medicion = lista[i]
            val medicionAnterior = lista[i - 1]
            medicion.valor = medicion.valor * ALPHA_FILTRO_PASA_BAJO + (1 - ALPHA_FILTRO_PASA_BAJO) * medicionAnterior.valor
        }

        return lista
    }

    companion object {
        const val CANTIDAD_MINIMA_DE_SATELITES_UTILIZADOS = 14
        private const val PRECISION_MINIMA = 1.25f
        private const val RITMO_DE_TRES_MINUTOS_POR_KILOMETRO = 180.0
        private const val RITMO_DE_OCHO_MINUTOS_POR_KILOMETRO = 480.0
        private const val ALPHA_FILTRO_PASA_BAJO = 0.4f
    }
}
